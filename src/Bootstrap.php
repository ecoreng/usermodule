<?php

namespace Base\UserModule;

use \Base\Extra\Concrete\PlatesView;
use \Base\Forms\Integrations\PlatesExtension;
use \Base\UserModule\Service\UserServices as Services;
use \Facebook\FacebookSession as FbSession;

class Bootstrap extends \Base\Extra\Concrete\ModuleBootstrap
{

    public function setup(array $settings)
    {
        parent::setup($settings);
        $this->checkRequirements();
        $this->loadConfigFile();
        $this->loadRouteFile();

        FbSession::setDefaultApplication(getenv('FB_APP_ID'), getenv('FB_APP_SECRET'));
        $this->loadServices(new Services([
            'fb-redirect-url' => $this->app->getConfig('usermodule.fb-redirect-url'),
            'auth-adapter' => $this->app->getConfig('usermodule.auth-adapter'),
            'auth-adapter-config' => $this->app->getConfig('usermodule.auth-adapter-config'),
            'saver' => $this->app->getConfig('usermodule.saver')
        ]));

        // Load the BaseForms Extension if it's not already loaded
        if (!$this->view->engine->doesFunctionExist('openForm')) {
            $this->view->engine->loadExtension(new PlatesExtension);
        }

        $this->view->engine->registerFunction('isTrue', function ($string) {
            return isset($string) ? ($string === true) : false;
        });
        
        $f = $this->getClassFolder() . 'View';
        $this->registerFolderIfExists($f, 'usermodule');
    }

    protected function registerFolderIfExists($folder, $name)
    {
        if (is_readable($folder)) {
            $this->view->engine->addFolder($name, $folder, true);
        }
    }

    protected function checkRequirements()
    {
        if (!($this->view instanceof PlatesView)) {
            throw new \Exception('Plates is required as the templating engine.');
        }

        if (!class_exists('Base\Forms\Integrations\PlatesExtension')) {
            throw new \Exception('Base\Forms\Integrations\PlatesExtension is required to handle the forms.');
        }
    }

}
