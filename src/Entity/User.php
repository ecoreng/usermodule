<?php
namespace Base\UserModule\Entity;

use \Spot\EntityInterface as Entity;
use \Spot\MapperInterface as Mapper;

class User extends \Spot\Entity
{
    protected static $table = 'users';

    public static function fields()
    {
        return [
            'id'                => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'email'             => ['type' => 'string', 'required' => true, 'unique' => true],
            'first_name'        => ['type' => 'string', 'required' => true],
            'last_name'         => ['type' => 'string', 'required' => true],
            'username'          => ['type' => 'string', 'required' => true, 'unique' => true],
            'password'          => ['type' => 'string'],
            'activation_code'   => ['type' => 'string'],
            'status'            => ['type' => 'integer', 'default' => 0]
        ];
    }

    /*
    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'tokens' => $mapper->hasMany($entity, 'ExampleCo\Example\Entity\Token', 'user_id')
                ->order(['id' => 'DESC']),
        ];
    }*/
}