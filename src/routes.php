<?php

return [
    'groups' => [
        'users' => [
            'route' => '/user',
            'group' => [
                'register' => [
                    'route' => '/register',
                    'controller' => '\Base\UserModule\Controller\UsersController:register',
                    'methods' => ['GET', 'POST']
                ],
                'logout' => [
                    'route' => '/logout',
                    'controller' => '\Base\UserModule\Controller\UsersController:logout',
                    'methods' => ['GET']
                ],
                'login' => [
                    'route' => '/login',
                    'controller' => '\Base\UserModule\Controller\UsersController:login',
                    'methods' => ['GET', 'POST']
                ],
                'activate' => [
                    'route' => '/activate',
                    'methods' => ['GET'],
                    'controller' => '\Base\UserModule\Controller\UsersController:getActivation',
                ],
                'forgotten' => [
                    'route' => '/forgotten',
                    'methods' => ['GET', 'POST'],
                    'controller' => '\Base\UserModule\Controller\UsersController:getForgotten',
                ],
                'reset' => [
                    'route' => '/reset',
                    'methods' => ['GET', 'POST'],
                    'controller' => '\Base\UserModule\Controller\UsersController:getReset',
                ],
                'social' => [
                    'route' => '/register-social',
                    'group' => [
                        'facebook' => [
                            'route' => '/facebook',
                            'controller' => '\Base\UserModule\Controller\FacebookController:registerFacebook',
                            'methods' => ['GET']
                        ],
                        'facebook-callback' => [
                            'route' => '/facebook-callback',
                            'controller' => '\Base\UserModule\Controller\FacebookController:registerFacebookCallback',
                            'methods' => ['GET']
                        ],
                        
                    ]
                ]
            ]
        ]
    ]
];
