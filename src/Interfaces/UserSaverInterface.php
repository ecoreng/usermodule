<?php

namespace Base\UserModule\Interfaces;

interface UserSaverInterface
{

    public function process($userData, $insert = true);

    public function setMapper(\Spot\Mapper $mapper);
}
