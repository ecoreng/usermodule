<?php

namespace Base\UserModule\Forms;

use Aura\Input\Form;

class RegisterFieldset extends Form
{

    public function init()
    {

        $this->setField('username', 'text')
            ->setAttribs([
                'maxlength' => 40,
            ]);

        $this->setField('email', 'text')
            ->setAttribs([]);

        $this->setField('password', 'password')
            ->setAttribs([
                'label' => 'Password'
            ]);

        $this->setField('repassword', 'password')
            ->setAttribs([
                'label' => 'Repeat Password'
            ]);

        $this->setField('first_name', 'text')
            ->setAttribs([
                'label' => 'First Name'
            ]);

        $this->setField('last_name', 'text')
            ->setAttribs([
                'label' => 'Last Name'
            ]);

        $this->setFilters();
    }

    public function setFilters()
    {
        $filter = $this->getFilter();

        $alphabetic = function ($value) {
            return ctype_alpha($value);
        };

        $filter->setRule(
            'first_name',
            'First name must be alphabetic only.',
            $alphabetic
        );
        $filter->setRule(
            'last_name',
            'Last name must be alphabetic only.',
            $alphabetic
        );

        $filter->setRule(
            'repassword',
            'The passwords must match.',
            function ($value, $fields) {
                return $value === $fields->password;
            }
        );
        
        $filter->setRule(
            'email',
            'Email must be valid',
            function ($value) {
                return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
            }
        );
        
    }

}
