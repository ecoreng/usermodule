<?php

namespace Base\UserModule\Forms;

use Aura\Input\Form;

class LoginFieldset extends Form
{

    public function init()
    {

        $this->setField('username', 'text')
            ->setAttribs([
                'maxlength' => 40,
            ]);

        $this->setField('password', 'password')
            ->setAttribs([
                'label' => 'Password'
            ]);

        $this->setFilters();
    }

    public function setFilters()
    {
        $filter = $this->getFilter();

        $alphabetic = function ($value) {
            return ctype_alpha($value);
        };

        $filter->setRule(
            'username',
            'Username must be alphabetic only.',
            $alphabetic
        );
        
        $filter->setRule(
            'password',
            'Password is required.',
            function ($value) {
                return $value === '';
            }
        );
    }
}
