<?php

namespace Base\UserModule\Forms;

use \Aura\Input\Form;

class ForgottenFieldset extends Form
{

    public function init()
    {

        $this->setField('email', 'text');

        $this->setFilters();
    }

    public function setFilters()
    {
        $filter = $this->getFilter();

        $filter->setRule('email', 'Invalid Email.',
            function ($value) {
            return filter_var($value, FILTER_VALIDATE_EMAIL);
        });
    }
}
