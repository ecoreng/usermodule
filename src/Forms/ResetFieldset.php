<?php

namespace Base\UserModule\Forms;

use \Aura\Input\Form;

class ResetFieldset extends Form
{

    public function init()
    {
        $this->setField('password', 'password');

        $this->setField('repassword', 'password')
            ->setAttribs([
                'label' => 'Retype Password'
        ]);

        $this->setField('code', 'hidden')
            ->setAttribs([
                'label' => false
        ]);

        $this->setField('user', 'hidden')
            ->setAttribs([
                'label' => false
        ]);


        $this->setFilters();
    }

    public function setFilters()
    {
        $filter = $this->getFilter();

        $filter->setRule('password',
            'Password must contain more than 6 characters, at least 1 Uppercase and 1 Lowercase character, a number and a symbol.',
            function($value) {
            $check = strlen($value) >= 6 &&
                preg_match("#[A-Z]+#", $value) &&
                preg_match("#[a-z]+#", $value) &&
                preg_match("#[0-9]+#", $value) &&
                preg_match("#[!\"\'\#\%\&\(\)\*\+\,\.\/\:\;\<\=\>\?\@\[\]\\\_\{\}\|\~\^\-]+#", $value);

            if (!$this->repassword) {
                if ($value !== '') {
                    return $check;
                }
                return true;
            }
            return $check;
        });

        $filter->setRule(
            'repassword', 'Passwords must match and not be empty.',
            function ($value, $fields) {
            return $value === $fields->password && $value !== '';
        }
        );

        $alphabetic = function ($value) {
            return ctype_alnum($value);
        };

        $filter->setRule(
            'code', 'Invalid Reset Password Code', $alphabetic
        );
        $filter->setRule(
            'user', 'Invalid user',
            function ($value) {
            return $value !== '' && $value !== null;
        }
        );
    }
}
