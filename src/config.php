<?php
return [
    'usermodule.success-login-redirect' => '/admin',
    'usermodule.success-login-facebook-redirect' => '/admin',

    'usermodule.success-register-redirect' => 'users_login',
    'usermodule.error-login-redirect' => 'users_login',
    'usermodule.after-activation-redirect' => 'users_login',
    'usermodule.failed-activation-redirect' => 'users_login',
    'usermodule.after-forgotten-redirect' => 'users_login',
    'usermodule.after-logout-redirect' => 'users_login',
    'usermodule.after-reset-redirect' => 'users_login',
    'usermodule.failed-reset-redirect' => 'users_login',
    'usermodule.error-login-facebook-redirect' => 'users_login',

    'usermodule.register-form' => 'Base\UserModule\Forms\RegisterFieldset',
    'usermodule.login-form' => 'Base\UserModule\Forms\LoginFieldset',

    'usermodule.user-entity' => 'Base\UserModule\Entity\User',
    'usermodule.saver' => 'Base\UserModule\Service\UserSaver',

    'usermodule.allow-public-registration' => true,
    
    'usermodule.social-login' => true,
    'usermodule.fb-redirect-url' => '/user/register-social/facebook-callback',
    'usermodule.facebook-login-scope' => ['email', 'public_profile'],

    'usermodule.register-view' => 'usermodule::Users/register',
    'usermodule.login-view' => 'usermodule::Users/login',
    'usermodule.forgotten-view' => 'usermodule::Users/forgotten',
    'usermodule.reset-view' => 'usermodule::Users/reset',
    'usermodule.social-partial' => 'usermodule::Users/register-social',
];
