<?php

namespace Base\UserModule\Controller;


use \Spot\Locator;
use \Base\EventEmitter as Emitter;
use \Base\Router;
use \Base\Config;
use \Base\Session;
use \Base\Controller;
use \Aura\Auth\Auth;
use \Interop\Container\ContainerInterface as DI;
use \Base\UserModule\Traits\RedirectCapable;
use \Base\UserModule\Traits\ConfigAware;
use \Base\UserModule\Traits\LocatorAware;
use \Base\UserModule\Traits\EmitterAware;
use \Base\Concrete\ControllerTrait;

class AppController implements Controller
{
    use ControllerTrait;
    use RedirectCapable;
    use ConfigAware;
    use LocatorAware;
    use EmitterAware;
    
    protected $session;
    protected $auth;
    protected $locator;
    protected $emitter;
    protected $randomizer;
    protected $config;
    protected $router;

    public function __construct(
        Router $router,
        Auth $auth,
        Session $session,
        Locator $locator,
        Emitter $emitter,
        DI $di,
        Config $config
    )
    {
        $this->session = $session;
        $this->auth = $auth;
        $this->locator = $locator;
        $this->emitter = $emitter;
        $this->randomizer = $di->get('random-string');
        $this->config = $config;
        $this->router = $router;
    }
}
