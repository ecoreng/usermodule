<?php

namespace Base\UserModule\Controller;

use \Base\UserModule\Controller\AppController;
use \Aura\Auth\Status;
use \Aura\Auth\Service\LoginService;
use \Base\UserModule\Service\FacebookRequestFactory as RequestFactory;
use \Base\UserModule\Interfaces\UserSaverInterface;
use \Facebook\FacebookRedirectLoginHelper as RedirectLogin;

class FacebookController extends AppController
{

    public function registerFacebook(RedirectLogin $rl)
    {
        if (!$this->get('usermodule.allow-public-registration')) {
            throw new \Phroute\Exception\HttpRouteNotFoundException("Route does not exist");
        }
        $this->session->start();
        $loginUrl = $rl->getLoginUrl($this->get('usermodule.facebook-login-scope'));
        return $this->redirect($loginUrl, 302);
    }

    public function registerFacebookCallback(
        RedirectLogin $rl,
        RequestFactory $rFactory,
        LoginService $login,
        UserSaverInterface $saver
    )
    {
        try {
            $this->session->start();
            $session = $rl->getSessionFromRedirect();
            if ($session) {
                $rFactory->setSession($session);
                $req = $rFactory->newRequest('/me');
                $gob = $req->execute()->getGraphObject();

                $mapper = $this->getMapper('usermodule.user-entity');

                $user = $mapper->where(['username' => 'fb-' . $gob->getProperty('email')])
                        ->execute();

                if (count($user) === 0) {
                    $userData = [
                        'first_name' => $gob->getProperty('first_name'),
                        'last_name' => $gob->getProperty('last_name'),
                        'password' => '',
                        'username' => 'fb-' . $gob->getProperty('email'),
                        'status' => 1,
                        'email' => $gob->getProperty('email')
                    ];
                    $saver->setMapper($mapper);
                    $user = $saver->process($userData);
                } else {
                    $user = $user->first();
                }

                if ($this->auth->getStatus() === Status::ANON) {
                    $username = $user->username;
                    $userdata = array(
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'email' => $user->email,
                    );
                    $login->forceLogin($this->auth, $username, $userdata);
                }
                // send them to the success-login-url
                return $this->redirectToConfig('usermodule.success-login-facebook-redirect');
            } else {
                throw new \Exception('An error ocurred');
            }
        } catch (\Exception $ex) {
            $this->session->setFlashNow('error', $ex->getMessage());
            return $this->redirectToConfig('usermodule.error-login-facebook-redirect');
        }
    }
}
