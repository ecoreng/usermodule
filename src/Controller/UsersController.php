<?php

namespace Base\UserModule\Controller;

use \Base\UserModule\Controller\AppController;
use \Base\Forms\CompoundFormFactory as CFF;
use \Aura\Auth\Service\LoginService;
use \Aura\Auth\Service\LogoutService;
use \Aura\Auth\Service\ResumeService;
use \Base\UserModule\Interfaces\UserSaverInterface;
use \Base\UserModule\Forms\ForgottenFieldset as Forgotten;
use \Base\UserModule\Forms\ResetFieldset as Reset;

class UsersController extends AppController
{

    public function logout(
        LogoutService $logout,
        ResumeService $resume
    )
    {
        try {
            $this->emit('usermodule.before.logout', []);
            $resume->resume($this->auth);
            $logout->logout($this->auth);
            $this->emit('usermodule.after.logout', []);
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }
        return $this->redirectToConfig('usermodule.after-logout-redirect');
    }

    public function login(LoginService $login, CFF $form)
    {
        if ($this->getRequest()->getMethod() === 'POST') {
            $userData = $this->getRequest()->getParsedBody();
            try {
                $this->emit('usermodule.before.login', $userData);
                $login->login($this->auth, $userData);
                $this->emit('usermodule.after.login', $userData);
                return $this->redirectToConfig('usermodule.success-login-redirect');
            } catch (\Exception $e) {
                $msg = "Invalid login details. Please try again : " . $e->getMessage();
            }
            $this->session->setFlashNow('error', $msg);
        }

        $loginForm = $form->addForm($this->get('usermodule.login-form'))->get();
        return $this->render(
            $this->get('usermodule.login-view'),
            [
                'loginForm' => $loginForm,
                'socialLoginEnabled' => $this->get('usermodule.social-login'),
                'socialPartial' => $this->get('usermodule.social-partial'),
            ]
        );
    }

    public function register(CFF $form, UserSaverInterface $saver)
    {
        if (!$this->get('usermodule.allow-public-registration')) {
            throw new \Phroute\Exception\HttpRouteNotFoundException("Route does not exist");
        }
        $registerForm = $form->addForm($this->get('usermodule.register-form'))->get();

        if ($this->getRequest()->getMethod() === 'POST') {
            $userData = $this->getRequest()->getParsedBody();
            $registerForm->fill($userData);
            if ($registerForm->filter()) {
                $mapper = $this->getMapper('usermodule.user-entity');
                $saver->setMapper($mapper);
                try {
                    $this->emit('usermodule.before.new-user', $userData);
                    $user = $saver->process($userData);
                    $this->emit('usermodule.after.new-user', $user);
                    return $this->redirectToConfig('usermodule.success-register-redirect');
                } catch (\Exception $ex) {
                    $this->session->setFlashNow('error', $ex->getMessage());
                }
            } else {
                $msg = "";
                $this->emit('usermodule.before.user-registration-invalid-field', [$registerForm, &$msg]);
                if ($msg === '') {
                    $msg = "User input is not valid." . PHP_EOL;
                    foreach ($registerForm->getMessages() as $name => $messages) {
                        foreach ($messages as $message) {
                            $msg .= "Input '{$name}': {$message}" . PHP_EOL;
                        }
                    }
                } else {
                    $msg = $msg->str;
                }
                $this->session->setFlashNow('error', $msg);
            }
        }

        return $this->render(
            $this->get('usermodule.register-view'),
            [
                'registerForm' => $registerForm,
                'socialLoginEnabled' => $this->get('usermodule.social-login'),
                'socialPartial' => $this->get('usermodule.social-partial'),
            ]
        );
    }

    public function getActivation()
    {
        if (!$this->get('usermodule.allow-public-registration')) {
            throw new \Phroute\Exception\HttpRouteNotFoundException("Route does not exist");
        }
        $req = $this->getRequest();
        $params = $req->getQueryParams();
        if (isset($params['code'])) {
            $code = $params['code'];
            if ($code) {
                $users = $this->getMapper('usermodule.user-entity');
                $user = $users->first(
                    [
                        'activation_code' => $code,
                        'status' => 0
                    ]
                );
                if ($user) {
                    $user->status = 1;
                    $user->activation_code = '';
                    $status = $users->update($user);
                    if ($status === 1) {
                        $this->session->setFlashNow('success', 'This user has been activated, you can now login.');
                        return $this->redirectToConfig('usermodule.after-activation-redirect');
                    }
                }
            }
            $this->session->setFlashNow('error', 'The provided activation code is invalid, or there is an error.');
        }
        return $this->redirectToConfig('usermodule.failed-activation-redirect');
    }

    public function getForgotten(Forgotten $forgotten)
    {
        $randomizer = $this->randomizer;
        $req = $this->getRequest();
        if ($req->getMethod() === 'POST') {
            $forgotten->fill($req->getParsedBody());
            if ($forgotten->filter()) {
                $email = $req->getParsedBody();
                $email = $email['email'];
                $users = $this->getMapper('usermodule.user-entity');
                $user = $users->first(['email' => $email]);
                if ($user) {
                    $user->activation_code = $randomizer(16);
                    $status = $users->update($user);
                    if ($status) {
                        $this->emit('usermodule.before.forgotten-password', $user);
                    }
                }
                $this->session->setFlashNow('success', 'If that email exists, a notification will be sent.');
                return $this->redirectToConfig('usermodule.after-forgotten-redirect');
            } else {
                $this->session->setFlashNow('error', 'That is not a valid email.');
            }
        }

        return $this->render(
            $this->get('usermodule.forgotten-view'),
            [
                'forgottenForm' => $forgotten
            ]
        );
    }

    public function getReset(Reset $resetForm, UserSaverInterface $saver)
    {
        $req = $this->getRequest();
        $params = $req->getQueryParams();
        if (isset($params['code']) && isset($params['user'])) {
            $code = $params['code'];
            $username = $params['user'];
            if ($code && $username) {
                $users = $this->getMapper('usermodule.user-entity');
                $user = $users->first(['username' => $username, 'activation_code' => $code]);
                if ($user) {
                    if ($req->getMethod() === 'POST') {
                        $params = $req->getParsedBody();
                        $resetForm->fill($params);
                        if ($resetForm->filter()) {
                            $saver->setMapper($users);
                            $user->password = $params['password'];
                            $user = $saver->process($user, false);
                            if ($user) {
                                $this->session->setFlashNow('success', 'Your password has ben reset');
                                return $this->redirectToConfig('usermodule.after-reset-redirect');
                            } else {
                                $this->session->setFlashNow('error', 'There has been an error and your password was not reset');
                            }
                        } else {
                            $msg = "Error:" . PHP_EOL;
                            foreach ($resetForm->getMessages() as $name => $messages) {
                                foreach ($messages as $message) {
                                    $msg .= "Field '{$name}': {$message}" . PHP_EOL;
                                }
                            }
                            $this->session->setFlashNow('error', $msg);
                        }
                    }
                    $resetForm->fill(['code' => $code, 'user' => $username]);
                    return $this->render(
                        $this->get('usermodule.reset-view'),
                        [
                            'resetForm' => $resetForm,
                            'code' => $code,
                            'username' => $username
                        ]
                    );
                }
            }
        }
        return $this->redirectToConfig('usermodule.failed-reset-redirect');
    }
}
