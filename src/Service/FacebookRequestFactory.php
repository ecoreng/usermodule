<?php

namespace Base\UserModule\Service;

use Facebook\FacebookRequest as Request;
use Facebook\FacebookSession as Session;

class FacebookRequestFactory
{

    protected $session;

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    public function newRequest($path, $httpMethod = 'GET', $session = null)
    {
        if ($session === null) {
            $session = $this->session;
        }
        return new Request($session, $httpMethod, $path);
    }
}
