<?php

namespace Base\UserModule\Service;

use \Base\UserModule\Interfaces\UserSaverInterface as Saver;

class UserSaver implements Saver
{

    /**
     * User Mapper
     * 
     * @var \Spot\Mapper
     */
    protected $mapper;

    public function process($userData, $insert = true)
    {
        if ($insert) {
            $user = $this->mapper->create([
                'first_name' => $userData['first_name'],
                'last_name' => $userData['last_name'],
                'password' => password_hash($userData['password'], PASSWORD_BCRYPT),
                'activation_code' => bin2hex(openssl_random_pseudo_bytes(16)),
                'username' => $userData['username'],
                'status' => 0,
                'email' => $userData['email']
            ]);
        } else {
            $pwdh = password_get_info($userData->password);
            if ($pwdh['algo'] === 0) {
                $userData->password = password_hash($userData->password, PASSWORD_BCRYPT);
                $userData->activation_code = '';
            }
            $user = $this->mapper->update($userData);
        }
        return $user;
    }

    public function setMapper(\Spot\Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

}
