<?php

namespace Base\UserModule\Service;

use \Base\ServiceRegisterer;
use \Facebook\FacebookRedirectLoginHelper as RedirectLogin;
use \Aura\Auth\AuthFactory;
use \Aura\Auth\Verifier\PasswordVerifier;
use \Interop\Container\ContainerInterface as IContainer;

class UserServices implements ServiceRegisterer
{
    protected $di;

    public function register(IContainer $di)
    {
        $this->di = $di;

        // Facebook SDK Configuration ==========================
        \Facebook\FacebookSession::setDefaultApplication(getenv('FB_APP_ID'), getenv('FB_APP_PW'));
        $di->set('Facebook\FacebookRedirectLoginHelper',
            function() use ($di) {
            $cfg = $di->get('Base\Config');
            $redirectUrl = $cfg->get('usermodule.fb-redirect-url');
            if (stripos($redirectUrl, 'http') !== 0) {
                $request = $di->get('Psr\Http\Message\RequestInterface');
                $host = $this->getHostUrl(parse_url($request->getUri()));
                $redirectUrl = $host . (substr($redirectUrl, 0, 1) !== '/' ? DS : '') . $redirectUrl;
            }
            return new RedirectLogin($redirectUrl);
        });

        $di->set('Base\UserModule\Interfaces\UserSaverInterface',
            function () use ($di) {
            $cfg = $di->get('Base\Config');
            $saver = $cfg->get('usermodule.saver');
            return $di->get($saver);
        });

        $di->set('random-string', $di->raw(function($length = 16){
            return bin2hex(openssl_random_pseudo_bytes($length));
        }));
    }

    private function getHostUrl($parsedUrl)
    {
        if (is_array($parsedUrl)) {
            $scheme = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
            $host = isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
            $port = isset($parsedUrl['port']) && $parsedUrl['port'] !== 80 ? ':' . $parsedUrl['port'] : '';
            $user = isset($parsedUrl['user']) ? $parsedUrl['user'] : '';
            $pass = isset($parsedUrl['pass']) ? ':' . $parsedUrl['pass'] : '';
            $pass = ($user || $pass) ? "$pass@" : '';
            return "$scheme$user$pass$host$port";
        }
        return '';
    }
}
