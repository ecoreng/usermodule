<?php

$flash = [
    'negative' => $this->session()->getFlash('error'),
    'positive' => $this->session()->getFlash('success')
];
$this->session()->clearAllFlash();
?>
<div class="iwrapper">
    <?php
    foreach ($flash as $type => $message):
        if ($message !== null):
            ?>
            <div class="ui <?= $type ?> message">
                <div class="header">
                    <?= $message ?>
                </div>
            </div>
            <?php
        endif;
    endforeach;
    ?>

    <?= $this->openForm(['action' => $this->getRoute('users_forgotten')]) ?>
    <?= $this->build($forgottenForm) ?>
    <input class="forgottern-btn" type="submit" value="Reset" />
    <?= $this->closeForm() ?>
</div>
<?php
$this->layout('Layouts/default', [
    'title' => 'Forgotten password',
    'head' => '',
    'templateName' => 'forgotten',
    'menuType' => 'simple button',
]);