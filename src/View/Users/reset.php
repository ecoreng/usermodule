<?php
$this->layout('Layouts/default', [
    'title' => 'Reset password',
    'head' => ''
]);
$flash = [
    'negative' => $this->session()->getFlash('error'),
    'positive' => $this->session()->getFlash('success')
];
$this->session()->clearAllFlash();
?>
<?php
foreach ($flash as $type => $message):
    if ($message !== null):
        ?>
        <div class="ui <?= $type ?> message">
            <div class="header">
                <?= $message ?>
            </div>
        </div>
        <?php
    endif;
endforeach;
?>


<?= $this->openForm(['action' => $this->getRoute('users_reset') . '?code=' . $code . '&user=' . $username]) ?>
<?= $this->build($resetForm) ?>
<input type="submit" value="Reset" />
<?= $this->closeForm() ?>

