<?php

$this->layout('Layouts/default', [
    'title' => 'Register New User',
    'head' => ''
]);
$flash = $this->session()->getFlashNext('error');
if ($flash !== null) {
    echo $flash;
}
?>

<?php if ($this->isTrue($socialLoginEnabled)) : ?>
    <?= $this->fetch($socialPartial) ?>
<?php endif; ?>

<?= $this->openForm(['action' => $this->getRoute('users_register')]) ?>
<?= $this->build($registerForm) ?>
<input type="submit" value="Register" />
<?= $this->closeForm() ?>