<?php

$this->layout('Layouts/default', [
    'title' => 'Login',
    'head' => ''
]);
$flash = $this->session()->getFlashNext('error');
if ($flash !== null) {
    echo $flash;
}
?>

<?php if ($this->isTrue($socialLoginEnabled)) : ?>
    <?= $this->fetch($socialPartial) ?>
<?php endif; ?>

<?= $this->openForm(['action' => $this->getRoute('users_login')]) ?>
<?= $this->build($loginForm) ?>
<input type="submit" value="Login" />
<?= $this->closeForm() ?>