<?php

namespace Base\UserModule\Traits;

trait EmitterAware
{

    protected function emit($event, $args)
    {
        if (!is_array($args)) {
            $args = [$args];
        }
        $this->emitter->emit($event, $args);
    }
}
