<?php

namespace Base\UserModule\Traits;

trait LocatorAware
{

    protected function getMapper($key, $config = true)
    {
        if ($config) {
            return $this->locator->mapper($this->config->get($key));
        }
        return $this->locator->mapper($key);
    }
}
