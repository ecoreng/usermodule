<?php

namespace Base\UserModule\Traits;

trait RedirectCapable
{

    protected function redirectToConfig($key, $code = 302)
    {
        return $this->redirect(
            $this->router->getRoute(
                $this->config->get($key)
            ),
            $code
        );
    }
}
