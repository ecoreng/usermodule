<?php

namespace Base\UserModule\Traits;

trait ConfigAware
{

    protected function get($key)
    {
        return $this->config->get($key);
    }
}
